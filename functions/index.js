// firebase functions:config:get > .runtimeconfig.json

///////////////
// LIBRARIES //
///////////////
// -------------------------------------------------------------------------------------------------
const functions = require('firebase-functions');
const admin = require('firebase-admin');
// -------------------------------------------------------------------------------------------------

//////////////////////////
// NETWORKING UTILITIES //
//////////////////////////
// -------------------------------------------------------------------------------------------------
const express = require('express');
const cookieParser = require('cookie-parser')();
const cors = require('cors')({origin: true});
const validateFirebaseToken = require('./crayon-api/utils/validateFirebaseToken');
// -------------------------------------------------------------------------------------------------

/////////////////////////
// INITIALIZE FIREBASE //
/////////////////////////
admin.initializeApp(functions.config().firebase);

///////////////////
// API ENDPOINTS //
///////////////////
// -------------------------------------------------------------------------------------------------
// configure api service
const apiService = express();
apiService.use(cors);
apiService.use(cookieParser);
apiService.use(validateFirebaseToken);

// define api endpoints
const createUserAccount = require('./crayon-api/endpoints/service/create-user-account');
apiService.post('/createUserAccount', createUserAccount);

// export configured service
exports.api = functions.https.onRequest(apiService);
// -------------------------------------------------------------------------------------------------

/////////////////////
// ADMIN ENDPOINTS //
/////////////////////
// -------------------------------------------------------------------------------------------------
// configure admin service
const adminService = express();

const createKeys = require('./crayon-api/endpoints/admin/create-keys');
adminService.post('/keys', createKeys);

const listKeys = require('./crayon-api/endpoints/admin/list-keys');
adminService.get('/keys', listKeys);

// initialize admin service
exports.admin = functions.https.onRequest(adminService);
// -------------------------------------------------------------------------------------------------