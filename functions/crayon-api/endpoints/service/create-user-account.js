const ACCOUNT_TYPES = require('../../constants/account-types');

const functions = require('firebase-functions');
const admin = require('firebase-admin');
const sequence = require('sequence-sdk');

module.exports = (req, res) => {
  const db = admin.firestore();
  const accounts = db.collection('accounts');
  const userId = req.user.user_id;
  const doc = accounts.doc(userId);

  doc.get().then(doc => {
    if(!doc.exists) {
      const sequenceConfig = functions.config().sequence;
      const ledger = new sequence.Client({
        ledger: sequenceConfig.ledger,
        credential: sequenceConfig.key
      });

      ledger.accounts.create({
        alias: userId,
        keys: [{ alias: 'INDIVIDUAL'}],
        tags: {
          type: ACCOUNT_TYPES.INDIVIDUAL
        }
      })
      .then((account) => {
        accounts.doc(userId).set({
          seq_id: account.id,
          type: ACCOUNT_TYPES.INDIVIDUAL
        });

        res.status(200).send({ success: 'account created' });
      })
      .catch((error) => {
        res.status(500).send({ error: error });
      });

    } else {
      res.status(403).send({ error: 'an account already exists for this user' });
    }
  });
}