const functions = require('firebase-functions');
const sequence = require('sequence-sdk');

module.exports = (req, res) => {
  const config = functions.config();
  const crayonConfig = config.crayon;
  const keys = req.body.keys;

  if(req.headers['crayon-authorization'] !== crayonConfig.authorization) {
    res.status(403).send({ error: 'not authorized'});
    return;
  }

  if(!keys || keys.length === 0) {
    res.status(400).send({ error: 'no keys provided' });
    return;
  }

  const sequenceConfig = config.sequence;
  const ledger = new sequence.Client({
    ledger: sequenceConfig.ledger,
    credential: sequenceConfig.key
  });

  const createPromise = Promise.all(keys.map((keyName) => {
    return ledger.keys.create({
      alias: keyName
    });
  }));

  createPromise
    .then(function() {
      res.status(200).send({ keys: keys });
    })
    .catch(function(error) {
      res.status(500).send({ error: error });
    });
};