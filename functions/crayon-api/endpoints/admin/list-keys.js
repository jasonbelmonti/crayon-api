const functions = require('firebase-functions');
const sequence = require('sequence-sdk');

module.exports = (req, res) => {
  const config = functions.config();
  const crayonConfig = config.crayon;

  if(req.headers['crayon-authorization'] !== crayonConfig.authorization) {
    res.status(403).send({ error: 'not authorized'});
    return;
  }

  const sequenceConfig = config.sequence;
  const ledger = new sequence.Client({
    ledger: sequenceConfig.ledger,
    credential: sequenceConfig.key
  });

  ledger.keys.queryAll()
    .then((keys) => {
      res.status(200).send(keys);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
};